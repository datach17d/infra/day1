#!/bin/bash

CLUSTER_NAME=${CLUSTER_NAME:-virt}
KEY_NAME=${KEY_NAME:-${CLUSTER_NAME}.flux.secrets.sops}
KEY_COMMENT=${KEY_COMMEND:-flux secrets for ${CLUSTER_NAME}}
BASE_DIR=$(git rev-parse --show-toplevel)
SOPS_VARS_DIR="ansible/inventory/host_vars/${CLUSTER_NAME}-controller"

if ! gpg --list-secret-keys "${KEY_NAME}"; then
gpg --batch --full-generate-key <<EOF
%no-protection
Key-Type: 1
Key-Length: 4096
Subkey-Type: 1
Subkey-Length: 4096
Expire-Date: 0
Name-Comment: ${KEY_COMMENT}
Name-Real: ${KEY_NAME}
EOF
fi

KEY_FP=$(gpg --list-secret-keys ${CLUSTER_NAME}.flux.secrets.sops | head -2 | tail -1 | sed 's/ //g')

if [ ! -f "${BASE_DIR}/.sups.pub.asc" ]; then
    gpg --export --armor "${KEY_FP}" > "${BASE_DIR}/.${CLUSTER_NAME}.sops.pub.asc"
fi

if [ ! -f "${BASE_DIR}/.sops.yaml" ]; then
cat <<EOF > "${BASE_DIR}/.sops.yaml"
creation_rules:
  - path_regex: ${SOPS_VARS_DIR}/.*.yaml
    pgp: ${KEY_FP}
EOF
fi

if [ ! -d "${BASE_DIR}/${SOPS_VARS_DIR}" ]; then

mkdir -p ${BASE_DIR}/${SOPS_VARS_DIR}

cat <<EOF > ${BASE_DIR}/${SOPS_VARS_DIR}/gitlab-sops-gpg.sops.yaml
sops.asc: |
$(gpg --export-secret-keys --armor "${KEY_NAME}" | sed 's/^/  /')
EOF

cat <<EOF > ${BASE_DIR}/${SOPS_VARS_DIR}/gitlab-day2-auth.sops.yaml
gitlab:
  username: ""
  password: ""
EOF

echo ${BASE_DIR}/${SOPS_VARS_DIR}/*.yaml | xargs -n1 sops --encrypt --in-place 

fi


