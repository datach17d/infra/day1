#!/bin/bash

kubectl --namespace=whome get secrets --kubeconfig /tmp/rke2.yaml whome-kubeconfig -o=jsonpath='{.data.value}' | base64 -d  > /tmp/whome.yaml
