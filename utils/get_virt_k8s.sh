#!/bin/bash

kubectl --namespace=virt get secrets --kubeconfig /tmp/rke2.yaml virt-kubeconfig -o=jsonpath='{.data.value}' | base64 -d  > /tmp/virt.yaml
