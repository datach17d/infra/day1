terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.6.14"
    }
    vbmc = {
      #source = "github.com/datachi7d/vbmc"
      #version = "0.3.1"
      source = "rgl/vbmc"
      version = "0.4.1"
    }
  }
}

variable "hosts" {
  default = 3
}

provider "libvirt" {
 uri = "qemu:///system"
}

resource "libvirt_volume" "node-disk" {
    name = "data-volume-${count.index}"
    pool = "default"
    count = var.hosts
    size = 20*1024*1024*1024
}

resource "libvirt_volume" "control-disk-base" {
    name = "data-volume-control-base"
    pool = "default"
    source = "https://gitlab.com/datach17d/infra/ubuntu-cloud-image/-/package_files/64991571/download"
    #source = "/home/sean/git/gitlab/infra/ubuntu-cloud-image/ubuntu.img"
}

resource "libvirt_volume" "control-disk" {
    name = "data-volume-control"
    pool = "default"
    base_volume_id = libvirt_volume.control-disk-base.id
    size = 20*1024*1024*1024
}

resource "libvirt_cloudinit_disk" "control_init" {
  name      = "commoninit.iso"
  user_data = data.template_file.control_init.rendered
}

data "template_file" "control_init" {
  template = file("${path.module}/control_init.cfg")
}

resource "libvirt_network" "mgmt" {
  name = "mgmt"
  bridge = "mgmt"
  mode = "nat"
  addresses = ["192.168.101.0/24"]
  dns {
    enabled = true
  }
  dhcp {
    enabled = true
  }
}

resource "libvirt_network" "baremetal" {
  name = "baremetal"
  bridge = "baremetal"
  mode = "nat"
  addresses = ["192.168.111.0/24"]
  dns {
    enabled = true
  }
  dhcp {
    enabled = true
  }
}

resource "libvirt_network" "provisioning" {
  name = "provisioning"
  bridge = "provisioning"
  mode = "none"
  addresses = ["172.22.0.0/24"]
  dhcp {
    enabled = false
  }
  dns {
    enabled = false
  }
  xml {
    xslt = <<EOF
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />   

      <xsl:template match="@*|node()">
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:template>

      <xsl:template match="@address[.='172.22.0.1']">
        <xsl:attribute name="address">172.22.0.2</xsl:attribute>
      </xsl:template>

    </xsl:stylesheet>
    EOF
  }
}

resource "vbmc_vbmc" "nodes" {
  domain_name = element(libvirt_domain.nodes.*.name, count.index)
  port = 6233 + count.index
  count = var.hosts
  address = "192.168.111.1"
  username = "admin"
  password = "password"
  depends_on = [ libvirt_domain.nodes ]
}

resource "libvirt_domain" "nodes" {
  name   = "node-${count.index}"
  autostart = false
  memory = "8192"
  vcpu   = 2
  running = false

  cpu {
    mode = "host-passthrough"
  }

  boot_device {
    dev = ["network"]
  }

  network_interface {
    network_id = libvirt_network.provisioning.id
    mac = "00:0${count.index + 1}:00:00:00:01"
  }

  network_interface {
    network_id = libvirt_network.baremetal.id
    mac = "00:0${count.index + 1}:00:00:00:02"
  }

  disk {
    volume_id = element(libvirt_volume.node-disk.*.id, count.index)
  }

  console {
    type = "tcp"
    target_type = "serial"
    target_port = "0"
    source_service = "${count.index + 12001}"
  }

  video {
    type = "none"
  }

  count = var.hosts
}


resource "libvirt_domain" "control" {
  name = "control"
  autostart = true
  memory = "8196"
  vcpu = 4
  running = true
 
  cloudinit = libvirt_cloudinit_disk.control_init.id

  cpu {
    mode = "host-passthrough"
  }

  network_interface {
    network_id = libvirt_network.mgmt.id
    mac = "00:00:00:00:00:10"
    addresses = ["192.168.101.5"]
    hostname = "rancher"
  }

  network_interface {
    network_id = libvirt_network.provisioning.id
    mac = "00:00:00:00:00:01"
    #addresses = ["172.22.0.1"]
  }

  network_interface {
    network_id = libvirt_network.baremetal.id
    mac = "00:00:00:00:00:02"
    addresses = ["192.168.111.10"]
  }

  disk {
    volume_id = libvirt_volume.control-disk.id
  }

  console {
    type = "tcp"
    target_type = "serial"
    target_port = "0"
    source_service = "12000"
  }

  video {
    type = "none"
  }
}
