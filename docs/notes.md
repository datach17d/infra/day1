
## Fixes to get rke2 running

```bash
# echo 'net.ipv4.conf.lxc*.rp_filter = 0' > /etc/sysctl.d/99-override_cilium_rp_filter.conf
# cat << EOF > /etc/sysctl.d/98-kubeadm.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
# echo "br_netfilter" > /etc/modules-load.d/bridge.conf
# systemctl restart systemd-modules-load.service
# systemctl restart systemd-sysctl
```
